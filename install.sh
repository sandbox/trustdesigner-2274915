#!/bin/bash
userDir='/usr/'
if [ ! -e $userDir ] || [ ! -d $userDir ]
then
    echo -e "Impossible d'accéder au répertorie /usr/\n"
    echo -e "Les clés seront conservés dans le répertoire du module shotnget\n"
    publicDir='./public/'
    if [ ! -e $publicDir ] || [ ! -d $publicDir ]
    then
        echo -e "Le dossier publique n'existe pas ou n'est pas accessible\n"
        echo -e "Veuillez activer le module Shotnget avant d'éxécuter ce script\n"
        exit
    fi
    privateDir='./private'
    if [ ! -e $privateDir ] || [ ! -d $privateDir ]
    then
        echo -e "Le dossier privé n'existe pas ou n'est pas accessible\n"
        echo -e "Veuillez activer le module Shotnget avant d'éxécuter ce script\n"
    fi
    chmod 440 './private/kpri'
else
    kpriDir='/usr/shotnget/private/'
    tempDir='/usr/shotnget/temp/'
    kpub='./public/kpub'
    kpri='./private/kpri'
    mkdir -pv $kpriDir
    mkdir -pv $tempDir
    chown www-data:www-data $tempDir
    if [ ! -e $kpub ] || [ ! -f $kpub ]
    then
        echo -e "La clé publique n'est pas généré\n"
        echo -e "Veuillez activer le module Shotnget avant d'éxécuter ce script\n"
        exit
    fi
    if [ ! -e $kpri ] || [ ! -f $kpri ]
    then
        echo -e "La clé privée n'est pas générée\n"
        echo -e "Veuillez activer le module Shotnget avant d'éxécuter ce script\n"
    fi
    mv -v $kpri $kpriDir
    chmod 440 '/usr/shotnget/private/kpri'
    chown www-data:www-data '/usr/shotnget/private/kpri'
    rm -rfv './private/'
    oldIFS=$IFS
    IFS=$'\n'
    file='./shotngetapi/config.php'
    newfile='./shotngetapi/config.tmp.php'
    if [ -e $newfile ]
    then
        rm -v $newfile
    fi
    touch $newfile
    for line in $(<$file)
    do
        IFS=$' '
        newline=""
        find="false"
        for word in $line
        do
            case $word in
                "\$TEMP_PATH")
                    newline="public static \$TEMP_PATH = '$tempDir';"
                    find="true"
                    ;;
                "\$KPRI")
                    newline="public static \$KPRI = '${kpriDir}kpri';"
                    find="true"
                    ;;
                *)
                    ;;
            esac
        done
        if [ $find = "false" ]
        then
            echo $line >> "$newfile"
        else
            echo $newline >> "$newfile"
        fi
    done
    mv $newfile $file
    chown -v www-data:www-data $file
fi
echo "Installation finalisée"