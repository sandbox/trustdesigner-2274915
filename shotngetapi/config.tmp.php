<?php
class Config {
public static $TEMP_PATH = '/usr/shotnget/temp/';
public static $RESPONSE_URL = 'http://192.168.3.80/Drupal7Test/sites/all/modules/shotnget/response_handler.php';
public static $TARGET_REDIRECT = 'http://192.168.3.80/Drupal7Test/sites/all/modules/shotnget/account.php';
public static $URLSIGN = '/var/www/Drupal7Test/sites/all/modules/shotnget/public/urlsign';
public static $OLDURLSIGN = '/var/www/Drupal7Test/sites/all/modules/shotnget/public/urlsign';
public static $KPUB = '/var/www/Drupal7Test/sites/all/modules/shotnget/public/kpub';
public static $KPRI = '/usr/shotnget/kpri';
public static $MODULE_INSTALL_PATH = 'sites/all/modules/shotnget';
public static $BASE_URL_DRUPAL = 'http://192.168.3.80/Drupal7Test';
public static $PLUGIN_NAME = 'shotnget';
public static $ACTIVE_MAIL = 'activation@shotnget.com';
}
?>
