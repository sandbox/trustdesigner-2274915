<?php

/**
 * Shotnget / es\SNG_LANG
 *
 * This module use the Shotnget API for generate a QRCode and provide a
 * new way for authentication to users.
 * Copyright (C) 2007-2014 Trust Designer, Blervaque David
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace es;
class SNG_LANG {
  public static $sng_help = 'Haga clic en el botón "SHOTNGET".
Escanear el QRCode con aplicación Shotnget en Android y iOS.';

  public static $sng_admin_mail_title = 
  'Texto del correo electrónico de registro.';

  public static $sng_admin_mail_desc = 
  'Este es el correo electrónico que se envía a los nuevos suscriptores.';

  public static $sng_menu_admin_desc = 
  'Esta página muestra el menú de configuración para el módulo Shotnget.';

  public static $sng_create_fail = 
  'Error: la creación de la cuenta no.';

  public static $sng_now_login = 'Ahora está conectado.';

  public static $sng_success_reg = 
  'Registro se realiza correctamente.';

  public static $sng_welcome_mail = 
  'Bienvenido a nuestro sitio web.';

  public static $sng_warning_reg = 
  'Su cuenta ha sido creada, pero el correo no pudo ser enviado.';

  public static $sng_install_error_openssl = 
  'OpenSSL para PHP debe estar instalado para que el módulo de Shotnget.';

  public static $sng_install_error_pkey_new = 
  'La función "openssl_pkey_new" no existe.';

  public static $sng_install_error_pkey_export = 
  'La función "openssl_pkey_export" no existe.';

  public static $sng_install_error_pkey_details = 
  'La función "openssl_pkey_get_details" no existe.';

  public static $sng_install_error_pkey_free = 
  'La función "openssl_pkey_free" no existe.';

  public static $sng_install_success_rsa_gen = 
  'Generación del par de claves RSA se ha realizado correctamente.';

  public static $sng_install_error_right_dir = 
  'El módulo de archivo Shotnget no se puede escribir. Cambie los permisos de carpeta.';

  public static $sng_install_command_help = 
  'Command: chown www-data:www-data <Directory>';

  public static $sng_install_create_pub_success = 
  'La creación de la carpeta "public" se ha realizado correctamente.';

  public static $sng_install_pub_exist = 
  'Ya existe la carpeta "public".';

  public static $sng_install_create_pub_fail = 
  'La creación de la carpeta "public" ha fallado.';

  public static $sng_install_create_pri_fail = 
  'La creación de la carpeta "private" ha fallado.';

  public static $sng_install_create_pri_success = 
  'La creación de la carpeta "private" se ha realizado correctamente.';

  public static $sng_install_pri_exist = 
  'Ya existe la carpeta "private".';

  public static $sng_install_fail_write_pri_file = 
  'La escritura del archivo de clave privada ha fallado. 
    Por favor, compruebe los permisos de archivo.';

  public static $sng_install_fail_open_pri_file = 
  'Al abrir el archivo para la clave privada ha fallado. 
    Por favor, compruebe los permisos de archivo.';

  public static $sng_install_write_pri_file_success = 
  'Se creó el archivo de clave privada.';

  public static $sng_install_pri_exist_ngen = 
  'El archivo de clave privada ya existe, pero no ha sido generado por Shotnget.
    Por favor, elimine antes de instalar.';

  public static $sng_install_pri_exist = 
  'Ya existe el archivo de clave privada.';

  public static $sng_install_fail_write_pub_file = 
  'Archivo de Escritura de la llave pública fracasó. 
    Por favor aud cificar los permisos de archivo.';

  public static $sng_install_fail_open_pub_file = 
  'Al abrir el archivo con la clave pública fracasó. 
    Por favor, compruebe los permisos de archivo.';

  public static $sng_install_write_pub_file_success = 
  'Se creó el archivo de clave pública.';

  public static $sng_install_pub_exist_ngen = 
  'El archivo de clave pública ya existe, pero no ha sido generado por Shotnget.
    Por favor, elimine antes de instalar.';

  public static $sng_install_pub_exist = 
  'Ya existe el archivo de clave pública.';

  public static $sng_install_mail_error = 
  'La dirección de correo electrónico de activación no se especifica en el 
    archivo de configuración.';

  public static $sng_install_mail_send_fail = 
  'El envío del correo electrónico fracasaron. 
    Por favor, haz clic en el enlace:';

  public static $sng_install_mail_send_success = 
  'El envío del correo electrónico se ha realizado correctamente.';

  public static $sng_install_api_error = 
  'La API Shotnget no está presente en la carpeta del módulo.';

  public static $sng_install_write_conf_file_fail = 
  'Escribiendo el archivo de configuración ha fallado.';

  public static $sng_install_open_conf_file_fail = 
  'Al abrir el archivo de configuración ha fallado.';

  public static $sng_install_done = 
  'La instalación se ha realizado correctamente.';
  
  public static $sng_install_detail_fail = 
  'La generación de datos de clave falló.';
  
  public static $sng_access_permission = 
  'El acceso a la página para autenticarse con Shotnget.';
  
  public static $sng_timeout = 
  'El módulo Shotnget no ha recibido una respuesta en el tiempo.';
  
  public static $sng_username_error = 
  'Su nombre de usuario no se encuentra en su teléfono inteligente.';
  
  public static $sng_password_error = 
  'La contraseña no se encuentra en su teléfono inteligente.';
  
  public static $sng_bad_username = 
  'Nombre de usuario incorrecto.';
  
  public static $sng_mail_error = 
  'Su dirección de correo electrónico no está configurado en 
    el teléfono inteligente.';
  
  public static $sng_bad_password = 'Contraseña incorrecta.';
  
  public static $sng_error_response = 'Conexión imposible. Error : ';
  
  public static $sng_success_page_mes = 'Mensaje de la página de éxito';
  
  public static $sng_success_page_desc = 
  'Dijo el mensaje que se mostrará después de Registrarse.';
  
  public static $sng_login_page_mes = 'Mensaje de la página de éxito (login)';
  
  public static $sng_login_page_desc = 
  'Dijo el mensaje que se mostrará después de la conexión del usuario.';
  
  public static $sng_error_disp = 'Error: Módulo Shotnget No se puede mostrar.';
  
  public static $sng_pass_change = 'La contraseña ha sido cambiada.';
  
  public static $sng_error_same_name = 'Este nombre de usuario ya está en uso.';
  
  public static $sng_error_same_mail = 
  'Esta dirección de correo electrónico ya está en uso.';
  
  public static $sng_error_block = 
  'El administrator de este sitio que haa bloqueado.';
  
  public static $sng_how_to_co =
  'Escanear el QRCode con aplicación Shotnget autenticar.';
  
  public static $sng_how_to_ch =
  'Escanear el QRCode con aplicación Shotnget para cambiar su contraseña.';
}
?>