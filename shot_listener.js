/* 
 * @file
 * Contain listening funcion for the Shotnget module
 * 
 */

function listening(rand, randPath, page, time, freq) {
    var clistener = new CListener(rand, randPath, page, time, freq);
    clistener.onFinishedListening = onFinishedListening;
    document.getElementById("shotnget_login_div").addEventListener("click", function(e) { clistener.close(); }, false);
    clistener.startListening();
}
 
function onFinishedListening(rand, error) {
    var listenerConst = new CListenerConst();
    var result = "";
    switch (error) {
        case listenerConst.RESULT_NO_ERROR:
	    var reg = new RegExp("(shotngetapi/waitresponse.php)", "g");
	    var newStr = rand.replace(reg, "account.php");
            window.location.replace(newStr);
            break;
        case listenerConst.RESULT_TIMEOUT:
            window.location.replace("?q=shotnget/shotnget_error/" + listenerConst.RESULT_TIMEOUT);
            break;
        case listenerConst.RESULT_RAND_ERROR:
            window.location.replace("?q=shotnget/shotnget_error/" + listenerConst.RESULT_RAND_ERROR);
            break;
        case listenerConst.RESULT_SERVER_ERROR:
            window.location.replace("?q=shotnget/shotnget_error/" + listenerConst.RESULT_SERVER_ERROR);
            break;
        case listenerConst.RESULT_PLUGIN_ERROR:
            window.location.replace("?q=shotnget/shotnget_error/" + listenerConst.RESULT_PLUGIN_ERROR);
            break;
        case listenerConst.RESULT_SERVER_ERROR_WITH_RETRY:
            window.location.replace("?q=shotnget/shotnget_error/" + listenerConst.RESULT_SERVER_ERROR_WITH_RETRY);
            break;
        case listenerConst.RESULT_CERTPHONE_ERROR:
            window.location.replace("?q=shotnget/shotnget_error/" + listenerConst.RESULT_CERTPHONE_ERROR);
            break;
        default:
            window.location.replace("index.php");
            break;
    }
}