<?php

/**
 * Shotnget / fr\SNG_LANG
 *
 * This module use the Shotnget API for generate a QRCode and provide a
 * new way for authentication to users.
 * Copyright (C) 2007-2014 Trust Designer, Gallet Kévin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace fr;
class SNG_LANG {
	public static $sng_help = 'Cliquez sur le bouton "SHOTNGET".
Flashez le QRCode avec l\'application Shotnget sur Android ou iOS.';

  public static $sng_admin_mail_title = 'Texte du courriel d\'inscription';

  public static $sng_admin_mail_desc = 
  'Ceci est le courriel qui sera envoyé à vos nouveaux inscrits.
    Compatible HTML.';

  public static $sng_menu_admin_desc = 
  'Cette page affiche le menu des paramètres pour le module Shotnget.';

  public static $sng_create_fail = 'Erreur : la création de compte a échoué.';

  public static $sng_now_login = 'Vous êtes maintenant connecté.';

  public static $sng_success_reg = 'Inscription réussie.';

  public static $sng_welcome_mail = 'Bienvenue sur note site web.';

  public static $sng_warning_reg = 
  'Votre compte a bien été créé mais le courriel n\'a pas pu être envoyé.';

  public static $sng_install_error_openssl = 
  'OpenSSL pour PHP doit être installé afin d\'activer le module Shotnget.';

  public static $sng_install_error_pkey_new = 
  'La fonction "openssl_pkey_new" n\'existe pas.';

  public static $sng_install_error_pkey_export = 
  'La fonction "openssl_pkey_export" n\'existe pas.';

  public static $sng_install_error_pkey_details = 
  'La fonction "openssl_pkey_get_details" n\'existe pas.';

  public static $sng_install_error_pkey_free = 
  'La fonction "openssl_pkey_free" n\'existe pas.';

  public static $sng_install_success_rsa_gen = 
  'La génération de la paire de clé RSA a réussie.';

  public static $sng_install_error_right_dir = 
  'Le dossier du module Shotnget n\'est pas accessible en écriture.
    Veuillez changer les droits du dossier.';

  public static $sng_install_command_help = 
  'Commande: chown www-data:www-data <Directory>';

  public static $sng_install_create_pub_success = 
  'La création du dossier "public" a réussie.';

  public static $sng_install_pub_exist = 'Le dossier "public" existe déjà.';

  public static $sng_install_create_pub_fail = 
  'La création du dossier "public" a échoué.';

  public static $sng_install_create_pri_fail = 
  'La création du dossier "private" a échoué.';

  public static $sng_install_create_pri_success = 
  'La création du dossier "private" a réussie.';

  public static $sng_install_pri_exist = 'Le dossier "private" existe déjà.';

  public static $sng_install_fail_write_pri_file = 
  'L\'écriture du fichier de la clé privée a échoué. 
    Veuillez vérifier les droits du fichier.';

  public static $sng_install_fail_open_pri_file = 
  'L\'ouverture du fichier de la clé privée a échoué.
    Veuillez vérifier les droits du fichier.';

  public static $sng_install_write_pri_file_success = 
  'Le fichier de la clé privée a été créé.';

  public static $sng_install_pri_exist_ngen = 
  'Le fichier de la clé privée existe déjà mais n\'a pas été 
    généré par Shotnget. Veuillez le supprimer avant 
    de procéder à l\'installation.';

  public static $sng_install_fail_write_pub_file = 
  'L\'écriture du fichier de la clé publique a échoué. 
    Veuillez vérifier les droits du fichier.';

  public static $sng_install_fail_open_pub_file = 
  'L\'ouverture du fichier de la clé publique a échoué. 
    Veuillez vérifier les droits du fichier.';

  public static $sng_install_write_pub_file_success = 
  'Le fichier de la clé publique a été créé.';

  public static $sng_install_pub_exist_ngen = 
  'Le fichier de la clé publique existe déjà mais n\'a pas été
    généré par Shotnget. Veuillez le supprimer avant de procéder
    à l\'installation.';

  public static $sng_install_fpri_exist = 
  'Le fichier de la clé publique existe déjà.';

  public static $sng_install_fpub_exist = 
  'Le fichier de la clé publique existe déjà.';

  public static $sng_install_mail_error = 
  'L\'addresse mail d\'activation n\'est pas indiqué dans le fichier 
    de configuration.';

  public static $sng_install_mail_send_fail = 
  'L\'envoie du mail a échoué. Veuillez cliquer sur le lien : ';

  public static $sng_install_mail_send_success = 
  'L\'envoie du mail a réussie.';

  public static $sng_install_api_error = 
  'L\'API Shotnget n\'est pas présente dans le dossier du module.';

  public static $sng_install_write_conf_file_fail = 
  'L\'écriture du fichier de configuration a échoué.';

  public static $sng_install_open_conf_file_fail = 
  'L\'ouverture du fichier de configuration a échoué.';

  public static $sng_install_done = 
  'L\'installation a réussie.';
	
	public static $sng_install_detail_fail = 
    'La génération des détails de la clé a échoué.';
	
	public static $sng_access_permission = 
  'Accès à la page permettant de s\'authentifier avec Shotnget.';
	
	public static $sng_timeout = 
  'Le module Shotnget n\'a pas reçu de réponse dans le temps.';
	
	public static $sng_username_error = 
  'Votre nom d\'utilisateur n\'est pas paramétré sur votre smartphone.';
	
	public static $sng_password_error = 
  'Votre mot de passe n\'est pas paramétré sur votre smartphone.';
	
	public static $sng_bad_username = 
  'Mauvais nom d\'utilisateur.';
	
	public static $sng_mail_error = 
  'Votre addresse email n\'est pas paramétré sur votre smartphone.';
	
	public static $sng_bad_password = 'Mauvais mot de passe';
	
	public static $sng_error_response = 'Connection impossible. Erreur : ';
	
	public static $sng_success_page_mes = 
  'Message de la page de succès (inscription)';
	
	public static $sng_success_page_desc = 
  'Indiqué le message qui sera affiché après l\'inscription.';
	
	public static $sng_login_page_mes = 
  'Message de la page de succès (connexion)';
	
	public static $sng_login_page_desc = 
  'Indiqué le message qui sera affiché après la connexion de l\'utilisateur.';
	
	public static $sng_error_disp = 
  'Erreur : le module Shotnget ne peut être affiché.';
	
	public static $sng_pass_change = 'Le mot de passe a été changé.';
	
	public static $sng_error_same_name = 
  'Ce nom d\'utilisateur est déjà utilisé.';
	
	public static $sng_error_same_mail = 'Cette adresse email est déjà utilisée.';
  
  public static $sng_error_block = 
  'L\'administrateur de ce site web vous a bloqué';
  
  public static $sng_how_to_co =
  'Flashez le QRCode avec l\'application Shotnget pour vous authentifier.';
  
  public static $sng_how_to_ch =
  'Flashez le QRCode avec l\'application Shotnget pour changer votre 
    mot de passe.';
}
?>