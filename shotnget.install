<?php

/**
 * Shotnget / shotnget.install
 *
 * This module use the Shotnget API for generate a QRCode and provide a
 * new way for authentication to users.
 * Copyright (C) 2007-2014 Trust Designer, Blervaque David
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function shotnget_install() {
  variable_set('kpubDone', FALSE);
  variable_set('kpriDone', FALSE);
}

function shotnget_enable() {
  $var_path = getcwd();
  $key_done = variable_get('keyDone');
  $openssl_loaded = extension_loaded('openssl');
  
  $lang = shotnget_reload(); //TODO TEST
  
  if ($openssl_loaded == FALSE) {
    drupal_set_message($lang::$sng_install_error_openssl, 'error', 
        TRUE);
    module_disabled(array('shotnget'), FALSE);
    return;
  }
  $openssl_func_exist = function_exists('openssl_pkey_new');
  if ($openssl_func_exist == FALSE) {
    drupal_set_message($lang::$sng_install_error_pkey_new, 'error', TRUE);
    module_disabled(array('shotnget'), FALSE);
    return;
  }
  $pk_generate = openssl_pkey_new(array(
    'private_key_bits' => 1024,
    'private_key_type' => OPENSSL_KEYTYPE_RSA,
  ));
  $openssl_func_exist = function_exists('openssl_pkey_export');
  if ($openssl_func_exist == FALSE) {
    drupal_set_message($lang::$sng_install_error_pkey_export, 'error', TRUE);
    module_disabled(array('shotnget'), FALSE);
    return;
  }
  openssl_pkey_export($pk_generate, $pk_generate_private);
  $openssl_func_exist = function_exists('openssl_pkey_get_details');
  if ($openssl_func_exist == FALSE) {
    drupal_set_message($lang::$sng_install_error_pkey_details, 'error', TRUE);
    module_disable(array('shotnget'), FALSE);
    return;
  }
  $pk_generate_details = openssl_pkey_get_details($pk_generate);
  if (!isset($pk_generate_details) || !isset($pk_generate_details['key'])) {
    drupal_set_message($lang::$sng_install_detail_fail, 'error', TRUE);
    module_disable(array('shotnget'), FALSE);
    return;
  }
  $pk_generate_public = $pk_generate_details['key'];
  $openssl_func_exist = function_exists('openssl_pkey_free');
  if ($openssl_func_exist == FALSE) {
    drupal_set_message($lang::$sng_install_error_pkey_free, 'error', TRUE);
    module_disable(array('shotnget'), FALSE);
    return;
  }
  openssl_pkey_free($pk_generate);
  drupal_set_message($lang::$sng_install_success_rsa_gen, 'status', TRUE);
  //Génération des clés publique et privé via openssl
  
  $shotnget_dir = drupal_get_path('module', 'shotnget');
  if (!is_writable($shotnget_dir)) {
    drupal_set_message($lang::$sng_install_error_right_dir, 'error', TRUE);
    drupal_set_message($lang::$sng_install_command_help . ' shotnget/',
        'error', TRUE);
    module_disable(array('shotnget'), FALSE);
    return;
  }
  $key_dir = $shotnget_dir . '/public';
  if (!is_dir($key_dir)) {
    if (!drupal_mkdir($key_dir, 0755, FALSE)) {
      drupal_set_message($lang::$sng_install_create_pub_fail, 'error', 
          TRUE);
      drupal_set_message(t('Please check the right of the shotnget module' .
          ' directory'), 'error', TRUE);
      module_disable(array('shotnget'), FALSE);
      return;
    }
    drupal_set_message($lang::$sng_install_create_pub_success,
        'status', TRUE);
  }
  else {
  drupal_set_message($lang::$sng_install_pub_exist,
      'status', TRUE);
  }
  //Création du répertoire contenant les clés
  
  $private_dir = $shotnget_dir . '/private';
  if (!is_dir($private_dir)) {
    if (!drupal_mkdir($private_dir, 0755, FALSE)) {
      drupal_set_message($lang::$sng_install_create_pri_fail, 'error',
          TRUE);
      drupal_set_message(t('Please check the right of the shotnget module' .
          ' directory.'), 'error', TRUE);
      module_disable(array('shotnget'), FALSE);
      return;
    }
    drupal_set_message($lang::$sng_install_create_pri_success,
        'status', TRUE);
  }
  else {
    drupal_set_message($lang::$sng_install_pri_exist, 'status', TRUE);
  }
  //Création du répertoire contenant la clé privé
  
  $private_key_path = $private_dir . '/kpri';
  $kpri_done = variable_get('kpriDone');
  if (!is_file($private_key_path)) {
    variable_set('krpiDone', FALSE);
    if (($fd = fopen($private_key_path, 'w'))) {
      if (!fwrite($fd, $pk_generate_private)) {
        drupal_set_message($lang::$sng_install_fail_write_pri_file, 'error',
            TRUE);
        module_disable(array('shotnget'), FALSE);
        return;
      }
    }
    else {
      drupal_set_message($lang::$sng_install_fail_open_pri_file, 'error', TRUE);
      module_disable(array('shotnget'), FALSE);
      return;
    }
    variable_set('kpriDone', TRUE);
    drupal_set_message($lang::$sng_install_write_pri_file_success, 'status',
        TRUE);
  }
  else {
  if ($kpri_done == FALSE) {
    drupal_set_message($lang::$sng_install_pri_exist_ngen, 'error', TRUE);
    return;
  }
  drupal_set_message($lang::$sng_install_fpri_exist, 'status', TRUE); }
  //Création du fichier contenant la clé privé.
  
  $new_contents = "";
  $array_kpub = explode("\n", $pk_generate_public);
  $j = 0;
  $newarray = array();
  for ($i = 0; $i < count($array_kpub); $i++) {
    if ($i > 0 && $i + 1 < count($array_kpub) - 1)
      $newarray[$j++] = $array_kpub[$i];
  }
  for ($i = 0; $i < count($newarray); $i++) {
    $new_contents .= $newarray[$i];
  }
  $new_contents = str_replace("\n", "", $new_contents);
  $pk_generate_public = $new_contents;
  //Modification de la clé publique

  $public_key_path = $key_dir . '/kpub';
  $kpub_done = variable_get('kpubDone');
  if (!is_file($public_key_path)) {
    variable_set('kpubDone', FALSE);
    if (($fd = fopen($public_key_path, 'w'))) {
      if (!fwrite($fd, $pk_generate_public)) {
        drupal_set_message($lang::$sng_install_fail_write_pub_file, 'error',
            TRUE);
        module_disable(array('shotnget'), FALSE);
        return;
      }
    }
    else {
      drupal_set_message($lang::$sng_install_fail_open_pub_file, 'error', TRUE);
      module_disable(array('shotnget'), FALSE);
      return;
    }
    variable_set('kpubDone', TRUE);
    drupal_set_message($lang::$sng_install_write_pub_file_success, 'status',
        TRUE);
  }
  else {
    if ($kpub_done == FALSE) {
      drupal_set_message($lang::$sng_install_pub_exist_ngen, 'error', TRUE);
      module_disable(array('shotnget'), FALSE);
      return;
    }
    drupal_set_message($lang::$sng_install_fpub_exist, 'status', TRUE);
  }
  //Création du fichier contenant la clé public
  
  $url = $GLOBALS['base_url'] . '/' . $public_key_path;
  $mail = variable_get('site_mail');
  $subject = "Shotnget Activation Demand";
  $message = "Nouvelle demande de signature:\r\nURL de la clé publique: " .
      $url . "\r\nDemande de: " . $mail . "\r\n";
  $headers = "From: " . $mail . "\r\n" .
      "Reply-To: " . $mail . "\r\n" .
      "X-Mailer: PHP/" . phpversion();
  $activ = Config::$ACTIVE_MAIL;
  if (!isset($activ) || $activ == "") {
    drupal_set_message($lang::$sng_install_mail_error, 'error', TRUE);
    module_disable(array('shotnget'), FALSE);
    return;
  }
  if (!mail($activ, $subject, $message, $headers)) {
    drupal_set_message($lang::$sng_install_mail_send_fail, 'warning', TRUE);
    drupal_set_message(check_plain('<a href="mailto:' . $activ . '?subject=' . 
        $subject . '&body=' . $message . '">Send mail</a>'), 'warning', TRUE);
  }
  else {
    drupal_set_message($lang::$sng_install_mail_send_success, 'status', TRUE);
  }
  //Envoie du mail de demande de signature
  
  $api_dir = $shotnget_dir . '/shotngetapi';
  if (!is_dir($api_dir)) {
    drupal_set_message($lang::$sng_install_api_error, 'error', TRUE);
    module_disable(array('shotnget'), FALSE);
    return;
  }
  $config_path = $api_dir . '/config.php';
  if (($fd = fopen($config_path, 'w'))) {
    $message = "<?php\n" .
        "class Config {\n" .
        "\n" .
        "public static \$TEMP_PATH = '" . $var_path . "/" . $key_dir . "/';\n" .
        "public static \$RESPONSE_URL = '" . $GLOBALS['base_url'] . "/" .
        $shotnget_dir . "/response_handler.php';\n" .
        "public static \$TARGET_REDIRECT = '" . $GLOBALS['base_url'] . "/" .
        $shotnget_dir . "/account.php';\n" .
        "public static \$URLSIGN = '" . $var_path . "/" . $key_dir . "/urlsign';\n" .
        "public static \$OLDURLSIGN = '" . $var_path . "/" . $key_dir .
        "/urlsign';\n" .
        "public static \$KPUB = '" . $var_path . "/" . $key_dir . "/kpub';\n" .
        "public static \$KPRI = '" . $var_path . "/" . $private_dir . "/kpri';\n" .
        "public static \$MODULE_INSTALL_PATH = '" . $shotnget_dir . "';\n" .
        "public static \$BASE_URL_DRUPAL = '" . $GLOBALS['base_url'] . "';\n" .
        "public static \$PLUGIN_NAME = 'shotnget';\n" .
        "public static \$ACTIVE_MAIL = 'activation@shotnget.com';\n" .
        "\n" .
        "}\n" .
        "?>";
    if (!fwrite($fd, $message)) {
      drupal_set_message($lang::$sng_install_write_conf_file_fail, 'error',
          TRUE);
      drupal_set_message($lang::$sng_install_command_help .
        ' shotngetapi/config.php', 'error', TRUE);
      module_disable(array('shotnget'), FALSE);
      return;
    }
  }
  else {
    drupal_set_message($lang::$sng_install_open_conf_file_fail, 'error', TRUE);
    drupal_set_message($lang::$sng_install_command_help .
      ' shotngetapi/config.php', 'error', TRUE);
    module_disable(array('shotnget'), FALSE);
    return;
  }
  drupal_set_message($lang::$sng_install_done, 'status', TRUE);
}

function shotnget_uninstall() {
  variable_del('krpiDone');
  variable_del('kpubDone');
}