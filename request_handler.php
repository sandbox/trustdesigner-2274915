<?php

/**
 * Shotnget / MyRequestManager
 *
 * This module use the Shotnget API for generate a QRCode and provide a
 * new way for authentication to users.
 * Copyright (C) 2007-2014 Trust Designer, Blervaque David
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class MyRequestManager extends CRequestManager {
  public function __construct() {
  }
  
  public function onInitRequest($initCmd, $requestParameters, &$request) {
    $userData = CUtils::getUserDataWithRequestParameters($requestParameters);
    $requestType = $userData->requestType;
    $file = new CFile(CFile::TYPE_WEBACCOUNT);
    $param = new CParam(CParam::TYPE_ID_UID);
    $opt = CParam::OPT_NEEDED.CParam::OPT_WRITABLE.CParam::OPT_UNIQUE;
    $param->addOpt($opt);
    $file->addParam($param);
    $param = new CParam(CParam::TYPE_PWD);
    $param->addOpt($opt);
    $file->addParam($param);
    $param = new CParam(CParam::TYPE_ID_MAIL);
    $param->addOpt($opt);
    $file->addParam($param);
    switch ($requestType) {
      case 'CONNECT':
        $cmd = new CCmdAuth(CCmdAuth::TYPEPWD_ALPHANUMERIC, 12, $file);
        $cmd->setLabel('WebAccount');
        $request->addCmd($cmd);
        break;
      case 'CHANGE':
        $cmd = new CCmdAuth(CCmdAuth::TYPEPWD_ALPHANUMERIC, 12, $file);
        $cmd->setLabel('ChangePass');
        $cmd->setOpt(CCmdAuth::OPT_CHANGE);
        $request->addCmd($cmd);
        break;
    }
  }

  public function onResponseReceived($response, $requestParameters, &$request) {
    $cmdResp = new CCmdResp();
    $cmdResp->setResult(CErrors::RESULT_NO_ERROR);
    $request->addCmd($cmdResp);
  }
}