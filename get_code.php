<?php

/**
 * Shotnget / get_code.php
 *
 * This module use the Shotnget API for generate a QRCode and provide a
 * new way for authentication to users.
 * Copyright (C) 2007-2014 Trust Designer, Blervaque David
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$apiPath = './shotngetapi/';
require ($apiPath.'shotnget_api.php');
$apiParameters = new ApiParameters($apiPath);
$apiManager = new ApiManager($apiParameters);
$responseUrl = Config::$RESPONSE_URL;
$isConnect = $_GET['isCo'];
$requestParameters = $apiManager->generateNewRequest($responseUrl,
    Config::$MODULE_INSTALL_PATH . '/' .
    'shotngetapi/waitresponse.php');
if ($requestParameters != null) {
  if ($isConnect == "1") {
    CUtils::setUserDataWithRequestParameters($requestParameters, 'requestType',
        'CHANGE');
  }
  else {
    CUtils::setUserDataWithRequestParameters($requestParameters, 'requestType',
        'CONNECT');
  }
  $imgPath = $requestParameters->getImgPath();
  $listeningFunction = $requestParameters->getListeningFunction();
  echo '<img id="shotnget_code" src="' . $imgPath . '" onload="' .
      $listeningFunction . '" /><br />';
}
else {
  echo $apiManager->getErrors();
}

?>
