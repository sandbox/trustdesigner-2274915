<?php

/**
 * Shotnget / du\SNG_LANG
 *
 * This module use the Shotnget API for generate a QRCode and provide a
 * new way for authentication to users.
 * Copyright (C) 2007-2014 Trust Designer, Blervaque David
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace du;
class SNG_LANG {
  public static $sng_help = 'Klik op de "SHOTNGET" knop.
Scan de QRCode met Shotnget applicatie op Android en iOS.';

  public static $sng_admin_mail_title = 
  'Tekst van de registratie e-mail.';

  public static $sng_admin_mail_desc = 
  'Dit is de e-mail die naar uw nieuwe abonnees wordt verstuurd.';

  public static $sng_menu_admin_desc = 
  'Deze pagina toont de menu-instellingen voor Shotnget module.';

  public static $sng_create_fail = 
  'Fout: aanmaken van een account is mislukt.';

  public static $sng_now_login = 
  'U bent nu verbonden.';

  public static $sng_success_reg = 
  'Succesvolle registratie.';

  public static $sng_welcome_mail = 
  'Welkom op onze website.';

  public static $sng_warning_reg = 
  'Uw account is aangemaakt, maar de e-mail kon niet worden verzonden.';

  public static $sng_install_error_openssl = 
  'OpenSSL voor PHP moet zijn geïnstalleerd om de Shotnget module te activeren.';

  public static $sng_install_error_pkey_new = 
  'De "openssl_pkey_new"-functie bestaat niet.';

  public static $sng_install_error_pkey_export = 
  'De functie "openssl_pkey_export" bestaat niet.';

  public static $sng_install_error_pkey_details = 
  'De functie "openssl_pkey_get_details" bestaat niet.';

  public static $sng_install_error_pkey_free = 
  'De functie "openssl_pkey_free" bestaat niet.';

  public static $sng_install_success_rsa_gen = 
  'Het genereren van de RSA-sleutelpaar was succesvol.';

  public static $sng_install_error_right_dir = 
  'Het bestand Shotnget module is niet beschrijfbaar. Wijzig de permissies map.';

  public static $sng_install_command_help = 
  'Command: chown www-data:www-data <Directory>';

  public static $sng_install_create_pub_success = 
  'De oprichting van de "public" map was succesvol.';

  public static $sng_install_pub_exist = 
  'De "public" map al bestaat.';

  public static $sng_install_create_pub_fail = 
  'De oprichting van de "public" map is mislukt.';

  public static $sng_install_create_pri_fail = 
  'De oprichting van de "private" map is mislukt.';

  public static $sng_install_create_pri_success = 
  'De oprichting van de map "private" was succesvol.';

  public static $sng_install_pri_exist = 'De map "private" bestaat reeds.';

  public static $sng_install_fail_write_pri_file = 
  'Het schrijven van het sleutelbestand prive mislukt. 
    Controleer de bestandsrechten.';

  public static $sng_install_fail_open_pri_file = 
  'Het openen van het bestand voor de persoonlijke sleutel is mislukt. 
    Controleer de bestandsrechten.';

  public static $sng_install_write_pri_file_success = 
  'De private key bestand is gemaakt.';

  public static $sng_install_pri_exist_ngen = 
  'Het belangrijkste dossier prive al bestaat, maar is niet gegenereerd door 
    Shotnget. Verwijder deze dan eerst alvorens te installeren.';

  public static $sng_install_pri_exist = 
  'Het belangrijkste dossier prive al bestaat.';

  public static $sng_install_fail_write_pub_file = 
  'Het schrijven van het dossier van de publieke sleutel is mislukt. 
    Controleer de bestandsrechten.';

  public static $sng_install_fail_open_pub_file = 
  'Het openen van het bestand met de publieke sleutel is mislukt. 
    Controleer de bestandsrechten.';

  public static $sng_install_write_pub_file_success = 
  'Het belangrijkste dossier openbaar is gemaakt.';

  public static $sng_install_pub_exist_ngen = 
  'Het sleutelbestand publiek reeds bestaat, maar is niet gegenereerd door 
    Shotnget. Verwijder deze dan eerst alvorens te installeren.';

  public static $sng_install_pub_exist = 
  'Het sleutelbestand publiek al bestaat.';

  public static $sng_install_mail_error = 
  'De activering mailadres wordt niet opgegeven in het configuratiebestand.';

  public static $sng_install_mail_send_fail = 
  'Het verzenden van de e-mail is mislukt. Klik op de link:';

  public static $sng_install_mail_send_success = 
  'Het verzenden van de e-mail was succesvol.';

  public static $sng_install_api_error = 
  'De Shotnget API is niet aanwezig in de map module.';

  public static $sng_install_write_conf_file_fail = 
  'Het configuratiebestand schrijven mislukt.';

  public static $sng_install_open_conf_file_fail = 
  'Het configuratiebestand openen mislukt.';

  public static $sng_install_done = 
  'De installatie was succesvol.';
  
  public static $sng_install_detail_fail = 
  'De generatie van de belangrijke details mislukt.';
  
  public static $sng_access_permission = 
  'Toegang tot de pagina om te verifiëren bij Shotnget.';
  
  public static $sng_timeout = 
  'De module Shotnget heeft geen antwoord ontvangen in de tijd.';
  
  public static $sng_username_error = 
  'Uw gebruikersnaam is niet ingesteld op uw smartphone.';
  
  public static $sng_password_error = 
  'Uw wachtwoord is niet ingesteld op uw smartphone.';
  
  public static $sng_bad_username = 
  'Verkeerde gebruikersnaam.';
  
  public static $sng_mail_error = 
  'Uw e-mailadres wordt niet op uw smartphone geconfigureerd.';
  
  public static $sng_bad_password = 
  'Verkeerd wachtwoord.';
  
  public static $sng_error_response = 
  'Verbinding onmogelijk. Fout : ';
  
  public static $sng_success_page_mes = 
  'Bericht van het succes pagina';
  
  public static $sng_success_page_desc = 
  'Zei het bericht moet worden weergegeven na aanmelding.';
  
  public static $sng_login_page_mes = 
  'Bericht van het succes pagina (login)';
  
  public static $sng_login_page_desc = 
  'Zei het bericht moet worden weergegeven nadat de gebruiker login.';
  
  public static $sng_error_disp = 
  'Fout: Shotnget module kan niet worden weergegeven.';
  
  public static $sng_pass_change = 
  'Het wachtwoord is gewijzigd.';
  
  public static $sng_error_same_name = 
  'Deze gebruikersnaam is al in gebruik.';
  
  public static $sng_error_same_mail = 
  'Dit e-mailadres is al in gebruik.';
  
  public static $sng_error_block = 
  'De beheereder van deze site heeft u gblockkeerd.';
  
  public static $sng_how_to_co =
  'Scan de QRCode met Shotnget toepassing te authenticeren.';
  
  public static $sng_how_to_ch =
  'Scan de QRCode met Shotnget applicatie om uw wachtwoord te wijzigen.';
}
?>