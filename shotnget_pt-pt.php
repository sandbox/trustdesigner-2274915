<?php

/**
 * Shotnget / pt_pt\SNG_LANG
 *
 * This module use the Shotnget API for generate a QRCode and provide a
 * new way for authentication to users.
 * Copyright (C) 2007-2014 Trust Designer, Blervaque David
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace pt_pt;
class SNG_LANG {
  public static $sng_help = 'Clique no botão "SHOTNGET".
Digitalizar o QRCode com aplicação Shotnget no Android e iOS.';

  public static $sng_admin_mail_title = 'Texto do e-mail de inscrição.';

  public static $sng_admin_mail_desc = 
  'Este é o e-mail que será enviado a seus novos assinantes.';

  public static $sng_menu_admin_desc = 
  'Esta página exibe o menu de configurações para o módulo Shotnget.';

  public static $sng_create_fail = 'Erro: criação de conta falhou.';

  public static $sng_now_login = 'Agora você está conectado.';

  public static $sng_success_reg = 
  'Registo bem sucedido.';

  public static $sng_welcome_mail = 'Bem-vindo ao nosso site.';

  public static $sng_warning_reg = 
  'Sua conta foi criada, mas o e-mail não pôde ser enviado.';

  public static $sng_install_error_openssl = 
  'OpenSSL para o PHP deve ser instalado para ativar o módulo Shotnget.';

  public static $sng_install_error_pkey_new = 
  'A função "openssl_pkey_new" não existe.';

  public static $sng_install_error_pkey_export = 
  'A função "openssl_pkey_export" não existe.';

  public static $sng_install_error_pkey_details = 
  'A função "openssl_pkey_get_details" não existe.';

  public static $sng_install_error_pkey_free = 
  'A função "openssl_pkey_free" não existe.';

  public static $sng_install_success_rsa_gen = 
  'Gerando o par de chaves RSA foi bem sucedida.';

  public static $sng_install_error_right_dir = 
  'O módulo de arquivo Shotnget não é gravável. Por favor, 
    altere as permissões da pasta.';

  public static $sng_install_command_help = 
  'Commande: chown www-data:www-data <Directory>';

  public static $sng_install_create_pub_success = 
  'A criação da pasta "public" foi bem sucedida.';

  public static $sng_install_pub_exist = 
  'A pasta "public" já existe.';

  public static $sng_install_create_pub_fail = 
  'A criação da pasta "public" falhou.';

  public static $sng_install_create_pri_fail = 
  'A criação da pasta "private" falhou.';

  public static $sng_install_create_pri_success = 
  'A criação da pasta "private" foi bem sucedida.';

  public static $sng_install_pri_exist = 
  'A pasta "public" já existe.';

  public static $sng_install_fail_write_pri_file = 
  'A escrita do arquivo de chave privada falhou. 
    Por favor, verifique as permissões do arquivo.';

  public static $sng_install_fail_open_pri_file = 
  'Abrindo o arquivo para a chave privada falhou. 
    Por favor, verifique as permissões do arquivo.';

  public static $sng_install_write_pri_file_success =
  'O arquivo de chave privada foi criado.';

  public static $sng_install_pri_exist_ngen =
  'O arquivo de chave privada já existe, mas não foi gerada por Shotnget.
    Por favor, remova-o antes de instalar.';

  public static $sng_install_pri_exist = 
  'Le fichier de la clé privée existe déjà.';

  public static $sng_install_fail_write_pub_file = 
  'Escrevendo arquivo da chave pública falhou. 
    Por favor, verifique as permissões do arquivo.';

  public static $sng_install_fail_open_pub_file = 
  'Abrindo o arquivo com a chave pública falhou.
    Por favor, verifique as permissões do arquivo.';

  public static $sng_install_write_pub_file_success = 
  'O arquivo de chave pública foi criado.';

  public static $sng_install_pub_exist_ngen = 
  'O arquivo de chave pública já existe, mas não foi gerada por Shotnget.
    Por favor, remova-o antes de instalar.';

  public static $sng_install_pub_exist = 
  'O arquivo de chave pública já existe.';

  public static $sng_install_mail_error = 
  'O endereço de correio ativação não for especificado no arquivo de configuração.';

  public static $sng_install_mail_send_fail = 
  'O envio do e-mail falhou. Por favor, clique no link:';

  public static $sng_install_mail_send_success = 
  'O envio do e-mail foi bem sucedida.';

  public static $sng_install_api_error = 
  'A API Shotnget não está presente na pasta módulo.';

  public static $sng_install_write_conf_file_fail = 
  'Escrevendo o arquivo de configuração falhou.';

  public static $sng_install_open_conf_file_fail = 
  'Abrindo o arquivo de configuração falhou.';

  public static $sng_install_done = 
  'A instalação foi bem sucedida.';
  
  public static $sng_install_detail_fail = 
  'A geração de detalhes importantes falhou.';
  
  public static $sng_access_permission = 
  'O acesso à página para autenticar com Shotnget.';
  
  public static $sng_timeout = 
  'O módulo Shotnget não recebeu uma resposta no tempo.';
  
  public static $sng_username_error = 
  'O seu nome de usuário não está definido em seu smartphone.';
  
  public static $sng_password_error = 
  'Sua senha não está definido em seu smartphone.';
  
  public static $sng_bad_username = 'Nome de usuário errado.';
  
  public static $sng_mail_error = 
  'Seu endereço de email não está configurado no seu smartphone.';
  
  public static $sng_bad_password = 'Senha errada.';
  
  public static $sng_error_response = 'Conexão impossível. Erro : ';
  
  public static $sng_success_page_mes = 'Mensagem da página de sucesso';
  
  public static $sng_success_page_desc = 
  'Said the message to be displayed after registration.';
  
  public static $sng_login_page_mes = 'Mensagem da página de sucesso (login)';
  
  public static $sng_login_page_desc = 
  'Disse que a mensagem a ser exibida após o login do usuário.';
  
  public static $sng_error_disp = 
  'Erro: módulo Shotnget não pode ser exibida.';
  
  public static $sng_pass_change = 'A senha foi alterada.';
  
  public static $sng_error_same_name = 'Este nome de usuário já está em uso.';
  
  public static $sng_error_same_mail = 
  'Este endereço de e-mail já está em uso.';
  
  public static $sng_error_block =
  'O administrator deste site bloqueou você.';
  
  public static $sng_how_to_co =
  'Digitalizar o QRCode com aplicação Shotnget para autenticar.';
  
  public static $sng_how_to_ch =
  'Digitalizar o QRCode com aplicação Shotnget para alterar sua senha.';
}
?>