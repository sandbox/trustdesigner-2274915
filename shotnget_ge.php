<?php

/**
 * Shotnget / ge\SNG_LANG
 *
 * This module use the Shotnget API for generate a QRCode and provide a
 * new way for authentication to users.
 * Copyright (C) 2007-2014 Trust Designer, Blervaque David
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ge;
class SNG_LANG {
public static $sng_help = 'Klicken Sie auf die "SHOTNGET"-Taste.
Scannen Sie den QRCode mit Shotnget Anwendung auf Android und iOS.';

public static $sng_admin_mail_title = 'Text der E-Mail-Registrierung.';

public static $sng_admin_mail_desc = 
'Dies ist der E-Mail, um Ihren neuen Abonnenten gesendet wird.';

public static $sng_menu_admin_desc = 
'Diese Seite zeigt das Einstellungsmenü für Shotnget-Modul.';

public static $sng_create_fail = 'Fehler: Kontoerstellung fehlgeschlagen.';

public static $sng_now_login = 'Sie sind jetzt verbunden.';

public static $sng_success_reg = 
'Erfolgreiche Registrierung.';

public static $sng_welcome_mail = 'Willkommen auf unserer Website.';

public static $sng_warning_reg = 
'Ihr Konto wurde erstellt, aber die E-Mail konnte nicht gesendet werden.';

public static $sng_install_error_openssl = 
'OpenSSL für PHP muss installiert werden, um das Modul zu aktivieren Shotnget.';

public static $sng_install_error_pkey_new = 
'Die Funktion "openssl_pkey_new" existiert nicht.';

public static $sng_install_error_pkey_export = 
'Die Funktion "openssl_pkey_export" existiert nicht.';

public static $sng_install_error_pkey_details = 
'Die Funktion "openssl_pkey_get_details" existiert nicht.';

public static $sng_install_error_pkey_free = 
'Die Funktion "openssl_pkey_free" existiert nicht.';

public static $sng_install_success_rsa_gen = 
'Die Erzeugung der RSA-Schlüsselpaar erfolgreich war.';

public static $sng_install_error_right_dir = 
'Die Datei Shotnget Modul ist nicht beschreibbar. Bitte ändern Sie die Ordner-Berechtigungen.';

public static $sng_install_command_help = 
'Command: chown www-data:www-data <Directory>';

public static $sng_install_create_pub_success = 
'Die Schaffung des "public"-Ordner war erfolgreich.';

public static $sng_install_pub_exist = 
'Der Ordner "public" ist bereits vorhanden.';

public static $sng_install_create_pub_fail = 
'Die Schaffung des "public" Ordner fehlgeschlagen.';

public static $sng_install_create_pri_fail = 
'Die Schaffung der "private" Ordner fehlgeschlagen.';

public static $sng_install_create_pri_success = 
'Die Schaffung der "private"-Ordner war erfolgreich.';

public static $sng_install_pri_exist = 
'Die "private" Ordner ist bereits vorhanden.';

public static $sng_install_fail_write_pri_file = 
'Das Schreiben der privaten Schlüsseldatei fehlgeschlagen. 
  Bitte überprüfen Sie die Dateiberechtigungen.';

public static $sng_install_fail_open_pri_file = 
'Das Öffnen der Datei für den privaten Schlüssel fehlgeschlagen. 
  Bitte überprüfen Sie die Dateiberechtigungen.';

public static $sng_install_write_pri_file_success = 
'Der private Schlüssel-Datei erstellt wurde.';

public static $sng_install_pri_exist_ngen = 
'Der private Schlüssel Datei bereits vorhanden ist, aber nicht von 
  Shotnget generiert. Bitte entfernen Sie diese vor der Installation.';

public static $sng_install_pri_exist = 
'Der öffentliche Schlüssel Datei existiert bereits.';

public static $sng_install_fail_write_pub_file = 
'Schreiben Datei der öffentliche Schlüssel fehlgeschlagen.
  Bitte überprüfen Sie die Dateiberechtigungen.';

public static $sng_install_fail_open_pub_file = 
'Öffnen der Datei mit dem öffentlichen Schlüssel fehlgeschlagen.
  Bitte überprüfen Sie die Dateiberechtigungen.';

public static $sng_install_write_pub_file_success = 
'Der öffentliche Schlüssel-Datei erstellt wurde.';

public static $sng_install_pub_exist_ngen = 
'Der öffentliche Schlüssel Datei bereits vorhanden ist, aber nicht
  von Shotnget generiert. Bitte entfernen Sie diese vor der Installation.';

public static $sng_install_pub_exist = 
'Der öffentliche Schlüssel Datei existiert bereits.';

public static $sng_install_mail_error = 
'Die Aktivierung Mail-Adresse ist nicht in der Konfigurationsdatei festgelegt.';

public static $sng_install_mail_send_fail = 
'Das Senden der E-Mail ist fehlgeschlagen. Bitte klicken Sie auf den Link:';

public static $sng_install_mail_send_success = 
'Das Senden der E-Mail war erfolgreich.';

public static $sng_install_api_error = 
'Die Shotnget API ist nicht im Modul Ordner vorhanden.';

public static $sng_install_write_conf_file_fail = 
'Schreiben Sie die Konfigurationsdatei fehlgeschlagen.';

public static $sng_install_open_conf_file_fail = 
'Das Öffnen der Konfigurationsdatei fehlgeschlagen.';

public static $sng_install_done = 'Die Installation war erfolgreich.';

public static $sng_install_detail_fail = 
'Die Erzeugung von Schlüssel Details gescheitert.';

public static $sng_access_permission = 
'Der Zugriff auf die Seite, um mit Shotnget authentifizieren.';

public static $sng_timeout = 
'Shotnget Modul eine Antwort in der Zeit empfangen.';

public static $sng_username_error = 
'Ihr Benutzername ist nicht auf Ihrem Smartphone eingerichtet.';

public static $sng_password_error = 
'Ihr Passwort wird nicht auf dem Smartphone einrichten.';

public static $sng_bad_username = 'Falscher Benutzername.';

public static $sng_mail_error = 
'Ihre E-Mail-Adresse wird nicht auf Ihrem Smartphone konfiguriert.';

public static $sng_bad_password = 'Falsches Passwort.';

public static $sng_error_response = 'Verbindung nicht möglich. Fehler : ';

public static $sng_success_page_mes = 'Nachricht aus der Erfolgsseite.';

public static $sng_success_page_desc = 
'Sagte der Nachricht nach der Anmeldung angezeigt werden.';

public static $sng_login_page_mes = 'Nachricht von dem Erfolg Seite (login)';

public static $sng_login_page_desc = 
'Sagte der Nachricht an den Benutzer nach der Anmeldung sichtbar sein.';

public static $sng_error_disp = 
'Fehler: Shotnget Modul kann nicht angezeigt werden.';

public static $sng_pass_change = 'Das Passwort wurde geändert.';

public static $sng_error_same_name = 
'Dieser Benutzername ist bereits im Einsatz.';

public static $sng_error_same_mail = 
'Diese E-Mail-Adresse ist bereits in Gebrauch.';

public static $sng_error_block =
'Der Administrator dieser Seite hat dich blockiert.';

public static $sng_how_to_co =
'Scannen Sie den QRCode mit Shotnget Applikation zu authentifizieren.';

public static $sng_how_to_ch =
'Scannen Sie den QRCode mit Shotnget Applikation, um Ihr Passwort zu ändern.';
}
?>